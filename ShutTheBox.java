import java.util.Scanner;

public class ShutTheBox {

	public static void main(String[] args){
		Scanner sc = new Scanner(System.in);
		String doContinue = "";
		int player1Points = 0;
		int player2Points = 0;

		System.out.println("Welcome to Shut the Box");
		
		do{
			Board b = new Board();
			Boolean gameOver = false;
			while(gameOver != true){
				System.out.println("Player 1's turn");
				System.out.println(b);
			
				if(b.playATurn()){
					System.out.println("Player 2 has won");
					gameOver = true;
					player2Points++;
				}else{ 
					System.out.println("Player 2's turn");
					System.out.println(b);
					
					if(b.playATurn()){
					System.out.println("Player 1 has won");
					gameOver = true;
					player1Points++;
					}

				}
			}
			System.out.println("Would you like to play again? (yes/no)");
			doContinue = sc.nextLine();
		} while(doContinue.equals("yes"));
		System.out.println("Player 1 points: " + player1Points);
		System.out.println("Player 2 points: " + player2Points);
	}
}
		

	
	