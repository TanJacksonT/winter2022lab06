public class Board {
	private Die firstDie;
	private Die secondDie;
	private boolean[] closedTiles;
	
	public Board() {
		this.firstDie = new Die();
		this.secondDie = new Die();
		this.closedTiles = new boolean[13];

}
	public String toString(){
		String empty = " ";
		String x = "X";
		for(int i = 1; i < closedTiles.length; i++){
			if( closedTiles[i] == false){
				empty += " " + i;
			}
			else{
				empty += " " + x;
			}
		}
		return empty;
	}

	public boolean playATurn(){
		this.firstDie.roll();
		this.secondDie.roll();
		System.out.println(this.firstDie);
		System.out.println(this.secondDie);
		int sum = this.firstDie.getPips() + this.secondDie.getPips();
		if(this.closedTiles[sum-1] == false){
			this.closedTiles[sum-1] = true;
			System.out.println("Closing tile: " + sum);
			return false; 
		}
		else{
			System.out.println("Tile is already shut");
			return true;
	}
}	
}	